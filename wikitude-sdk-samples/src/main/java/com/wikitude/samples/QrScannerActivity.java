package com.wikitude.samples;

import android.Manifest;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;

import com.wikitude.sdksamples.R;

import github.nisrulz.qreader.QRDataListener;
import github.nisrulz.qreader.QREader;

public class QrScannerActivity extends AppCompatActivity {

    private SurfaceView mySurfaceView;
    private QREader qrEader;
    private boolean blocked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_scanner);

        mySurfaceView = (SurfaceView) findViewById(R.id.camera_view);

        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.CAMERA},
                1
        );

        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.initiateScan();

        /*qrEader = new QREader.Builder(this, mySurfaceView, new QRDataListener() {
            @Override
            public void onDetected(final String data) {
                Log.d("QREader", "Value : " + data);
                *//*text.post(new Runnable() {
                    @Override
                    public void run() {
                        text.setText(data);
                    }
                });*//*
            }
        }).facing(QREader.BACK_CAM)
                .enableAutofocus(true)
                .height(mySurfaceView.getHeight())
                .width(mySurfaceView.getWidth())
                .build();*/
    }

    /*@Override
    protected void onResume() {
        super.onResume();

        // Init and Start with SurfaceView
        // -------------------------------
        qrEader.initAndStart(mySurfaceView);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Cleanup in onPause()
        // --------------------
        qrEader.releaseAndCleanup();
    }*/

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if(blocked) return;
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {
            blocked = true;
            Intent uploadIntent = new Intent(this, UploadActivity.class);
            uploadIntent.putExtra("CONTENT", scanResult.getContents());
            startActivity(uploadIntent);
        }
        else
            Log.d("QrCode", "Failed");
    }
}
